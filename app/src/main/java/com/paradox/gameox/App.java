package com.paradox.gameox;

import android.app.Application;

/**
 * Created by estebanlopez on 8/28/15.
 */
public class App extends Application {



    private static boolean activityVisible;

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }


    @Override
    public void onCreate() {
        super.onCreate();



    }

    @Override
    public void onTerminate() {
        super.onTerminate();

    }
}
