package com.paradox.gameox.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by estebanlopez on 8/25/15.
 */
public class GameState implements Serializable{

    private int id;
    private int playerOneId;
    private int playerTwoId;
    private Character posO;
    private Character pos1;
    private Character pos2;
    private Character pos3;
    private Character pos4;
    private Character pos5;
    private Character pos6;
    private Character pos7;
    private Character pos8;
    private Date created_on;

    private int currentTurn;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlayerOneId() {
        return playerOneId;
    }

    public void setPlayerOneId(int playerOneId) {
        this.playerOneId = playerOneId;
    }

    public int getPlayerTwoId() {
        return playerTwoId;
    }

    public void setPlayerTwoId(int playerTwoId) {
        this.playerTwoId = playerTwoId;
    }

    public Character getPosO() {
        return posO;
    }

    public void setPosO(Character posO) {
        this.posO = posO;
    }

    public Character getPos1() {
        return pos1;
    }

    public void setPos1(Character pos1) {
        this.pos1 = pos1;
    }

    public Character getPos2() {
        return pos2;
    }

    public void setPos2(Character pos2) {
        this.pos2 = pos2;
    }

    public Character getPos3() {
        return pos3;
    }

    public void setPos3(Character pos3) {
        this.pos3 = pos3;
    }

    public Character getPos4() {
        return pos4;
    }

    public void setPos4(Character pos4) {
        this.pos4 = pos4;
    }

    public Character getPos5() {
        return pos5;
    }

    public void setPos5(Character pos5) {
        this.pos5 = pos5;
    }

    public Character getPos6() {
        return pos6;
    }

    public void setPos6(Character pos6) {
        this.pos6 = pos6;
    }

    public Character getPos7() {
        return pos7;
    }

    public void setPos7(Character pos7) {
        this.pos7 = pos7;
    }

    public Character getPos8() {
        return pos8;
    }

    public void setPos8(Character pos8) {
        this.pos8 = pos8;
    }

    public Date getCreated_on() {
        return created_on;
    }

    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }

    public int getCurrentTurn() {
        return currentTurn;
    }

    public void setCurrentTurn(int currentTurn) {
        this.currentTurn = currentTurn;
    }
}
