package com.paradox.gameox.model;

/**
 * Created by estebanlopez on 8/28/15.
 */
public class GameHelper {

    public static boolean isWinner(Character jugador, Character[][]tablero){

        final boolean primeraLineaVertical=(tablero[0][0]==jugador & tablero[1][0]==jugador & tablero[2][0]==jugador);

        final boolean segundaLineaVertical=(tablero[0][1]==jugador & tablero[1][1]==jugador & tablero[2][1]==jugador);

        final boolean terceraLineaVertical=(tablero[0][2]==jugador & tablero[1][2]==jugador & tablero[2][2]==jugador);

        final boolean verticales=primeraLineaVertical || segundaLineaVertical || terceraLineaVertical;

        final boolean primeraLineaHorizontal=(tablero[0][0]==jugador & tablero[0][1]==jugador & tablero[0][2]==jugador);

        final boolean segundaLineaHorizontal=(tablero[1][0]==jugador & tablero[1][1]==jugador & tablero[1][2]==jugador);

        final boolean terceraLineaHorizontal=(tablero[2][0]==jugador & tablero[2][1]==jugador & tablero[2][2]==jugador);

        final boolean horizontales=  primeraLineaHorizontal || segundaLineaHorizontal || terceraLineaHorizontal;

        final boolean diagonalDescendente=(tablero[0][0]==jugador & tablero[1][1]==jugador & tablero[2][2]==jugador);

        final boolean diagonalAscendente=(tablero[0][2]==jugador & tablero[1][1]==jugador & tablero[2][0]==jugador);



        return (verticales ||horizontales||diagonalAscendente||diagonalDescendente);

    }
}
