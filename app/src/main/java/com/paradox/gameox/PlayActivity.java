package com.paradox.gameox;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.paradox.gameox.model.GameHelper;
import com.paradox.gameox.model.GameState;
import com.paradox.gameox.model.Player;
import com.paradox.gameox.utils.QueryUtils;

public class PlayActivity extends AppCompatActivity implements View.OnClickListener {

    static Character PLAYER_ONE_INDICATOR = 'X';
    static Character PLAYER_TWO_INDICATOR = 'O';

    public static final int INSERT_GAME_STATE = 1;
    public static final int UPDATE_GAME_STATE = 2;


    TimeDelayAsyncTask timeDelayAsyncTask;

    Player playerOne;
    Player playerTwo;

    boolean isPlaying = false;

    Button button0,button1,button2,button3,button4,button5,button6,button7, button8;

    //player information
    TextView tvCurrentPlay, tvTimeLapse, tvPlayerOneNickname, tvPlayerOneWins, tvPlayerOneLost, tvPlayerTwoNickname, tvPlayerTwoWins, tvPlayerTwoLost;
    CountDownTimer timer;

    Button [][] gameMatrixButton = new Button[3][3];
    Character [][] gameMatrix = new Character[3][3];

    private GameState gameState;

    int currentPlayerTurn = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        initComponents();

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            gameState = (GameState)extras.getSerializable("gameState");

            if(gameState != null) {
                checkParamsAndLoadGame(true);
            }else{
                playerOne = (Player)extras.getSerializable("playerOne");
                playerTwo = (Player)extras.getSerializable("playerTwo");
                gameState = insertAndUpdateGame(INSERT_GAME_STATE);
                checkParamsAndLoadGame(false);
            }
        }


    }

    private GameState insertAndUpdateGame(int INSERT_OR_UPDATE) {

        if(INSERT_OR_UPDATE == INSERT_GAME_STATE){
            QueryUtils.insertGameState(getApplicationContext(), gameMatrix, playerOne.getId(), playerTwo.getId(), currentPlayerTurn);
        }else if(INSERT_OR_UPDATE == UPDATE_GAME_STATE){
            QueryUtils.updateGameState(getApplicationContext(), gameMatrix, currentPlayerTurn, gameState);
        }
        GameState gameState =  QueryUtils.getGameState(getApplicationContext());
        return gameState;

    }

    /*
    Metodo para obtener estado del juego y cargarlo
     */
    private void checkParamsAndLoadGame(boolean isGameState) {




        if(isGameState) {
            currentPlayerTurn = gameState.getCurrentTurn();
            playerOne = QueryUtils.findPlayerByNicknameOrId(getApplicationContext(), gameState.getPlayerOneId(), int.class);
            playerTwo = QueryUtils.findPlayerByNicknameOrId(getApplicationContext(), gameState.getPlayerTwoId(), int.class);
        }

            tvPlayerOneNickname.setText(playerOne.getNickname());
            tvPlayerOneWins.setText("Wins: "+playerOne.getWin());
            tvPlayerOneLost.setText("Lost: "+playerOne.getLost());

            tvPlayerTwoNickname.setText(playerTwo.getNickname());
            tvPlayerTwoWins.setText("Wins: "+playerTwo.getWin());
            tvPlayerTwoLost.setText("Lost: "+playerTwo.getLost());

        if(!isGameState)
            currentPlayerTurn = 0;


            if(currentPlayerTurn == 0)
                tvCurrentPlay.setText(String.valueOf(PLAYER_ONE_INDICATOR));
            else
                tvCurrentPlay.setText(String.valueOf(PLAYER_TWO_INDICATOR));


        if(isGameState){


            setValuesMatrixItem(gameMatrix,0,0, gameState.getPosO());
            setValuesMatrixItem(gameMatrix,0,1, gameState.getPos1());
            setValuesMatrixItem(gameMatrix,0,2, gameState.getPos2());
            setValuesMatrixItem(gameMatrix,1,0, gameState.getPos3());
            setValuesMatrixItem(gameMatrix,1,1, gameState.getPos4());
            setValuesMatrixItem(gameMatrix,1,2, gameState.getPos5());
            setValuesMatrixItem(gameMatrix,2,0, gameState.getPos6());
            setValuesMatrixItem(gameMatrix,2,1, gameState.getPos7());
            setValuesMatrixItem(gameMatrix,2,2, gameState.getPos8());

        }

        updateMatrixUI();




    }

    private void setValuesMatrixItem(Character[][] gameMatrix, int posi, int posx, Character val) {
        if(val !=null && val != 'n'){
            gameMatrix[posi][posx] = val.charValue();
        }
    }


    private void updateMatrixUI() {
        for(int x = 0 ; x < gameMatrixButton.length ; x++){
            for(int i = 0 ; i < gameMatrixButton.length ; i++){
                if(gameMatrix[x][i]!=null) {
                    Character value = gameMatrix[x][i];
                    if(gameMatrixButton[x][i]!=null){
                       gameMatrixButton[x][i].setText(value+"");
                    }

                }
            }
        }
    }

    private void initComponents() {


        tvCurrentPlay =(TextView) findViewById(R.id.tvCurrentPlay);
        tvPlayerOneNickname =(TextView) findViewById(R.id.tvPlayerOne);
        tvPlayerOneWins =(TextView) findViewById(R.id.tvPlayerOneWins);
        tvPlayerOneLost =(TextView) findViewById(R.id.tvPlayerOneLost);




        tvPlayerTwoNickname =(TextView) findViewById(R.id.tvPlayerTwo);
        tvPlayerTwoWins =(TextView) findViewById(R.id.tvPlayerTwoWins);
        tvPlayerTwoLost =(TextView) findViewById(R.id.tvPlayerTwoLost);


        tvTimeLapse =(TextView) findViewById(R.id.tvTimeLapse);

        tvCurrentPlay.setText(PLAYER_ONE_INDICATOR+"");

        button0 = (Button)findViewById(R.id.button0);
        button0.setTag("0-0");
        button1 = (Button)findViewById(R.id.button1);
        button1.setTag("0-1");
        button2 = (Button)findViewById(R.id.button2);
        button2.setTag("0-2");
        button3 = (Button)findViewById(R.id.button3);
        button3.setTag("1-0");
        button4 = (Button)findViewById(R.id.button4);
        button4.setTag("1-1");
        button5 = (Button)findViewById(R.id.button5);
        button5.setTag("1-2");
        button6 = (Button)findViewById(R.id.button6);
        button6.setTag("2-0");
        button7 = (Button)findViewById(R.id.button7);
        button7.setTag("2-1");
        button8 = (Button)findViewById(R.id.button8);
        button8.setTag("2-2");

        button0.setOnClickListener(this);
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        button7.setOnClickListener(this);
        button8.setOnClickListener(this);

        gameMatrixButton[0][0] = button0;
        gameMatrixButton[0][1] = button1;
        gameMatrixButton[0][2] = button2;
        gameMatrixButton[1][0] = button3;
        gameMatrixButton[1][1] = button4;
        gameMatrixButton[1][2] = button5;
        gameMatrixButton[2][0] = button6;
        gameMatrixButton[2][1] = button7;
        gameMatrixButton[2][2] = button8;

        restartGame();


        initTimer();
    }

    private void initTimer() {

        isPlaying = true;
        if(timeDelayAsyncTask!=null)
            timeDelayAsyncTask.cancel(true);

        timeDelayAsyncTask = null;

        timeDelayAsyncTask = new TimeDelayAsyncTask();
        timeDelayAsyncTask.execute();

      /* if( timer!=null){
           timer.cancel();
       }
        timer = null;
         timer = new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
                tvTimeLapse.setText("00:" + millisUntilFinished / 1000);
            }

            public void onFinish() {
              //  tvTimeLapse.setText("Turno perdido");
                if (currentPlayerTurn == 0) {
                    tvCurrentPlay.setText(String.valueOf(PLAYER_ONE_INDICATOR));
                    currentPlayerTurn =  0;
                } else {
                    tvCurrentPlay.setText(String.valueOf(PLAYER_TWO_INDICATOR));
                    currentPlayerTurn =  1;
                }
                initTimer();

            }
        }.start();*/

    }


    private class TimeDelayAsyncTask extends AsyncTask<Void, Integer, Integer> {
        @Override
        protected Integer doInBackground(Void... unused) {
            int i = 10;
            while (isPlaying) {

                publishProgress(i);
                SystemClock.sleep(1000);
                i--;
                if(i == 0) {
                    i = 10;
                    isPlaying = false;

                }

            }
            return i;
        }

        protected void onProgressUpdate(Integer... index) {
            //getWindow().getDecorView().setBackgroundColor(colorNumberarray[index[0]]);
            tvTimeLapse.setText("00:0" + index[0]);
        }

        protected void onPostExecute(Integer result) {
            checkTurn();
        }
    }

    private void checkTurn() {
    //    tvTimeLapse.setText("Turno perdido");
        if (currentPlayerTurn == 0) {
            currentPlayerTurn =  1;
            tvCurrentPlay.setText(String.valueOf(PLAYER_TWO_INDICATOR));

        } else {
            currentPlayerTurn =  0;
            tvCurrentPlay.setText(String.valueOf(PLAYER_ONE_INDICATOR));

        }
        gameState.setCurrentTurn(currentPlayerTurn);
        insertAndUpdateGame(UPDATE_GAME_STATE);
        initTimer();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_play, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            logout();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        QueryUtils.deleteGameState(getApplicationContext(), gameState);
        finish();
    }

    @Override
    public void onClick(View v) {
        checkButtonAndPlayerValue(v);
    }

    private void checkButtonAndPlayerValue(View v) {

        Button btn =(Button) findViewById(v.getId());
        if(canWrite(btn)) {
            isPlaying = false;
            if (currentPlayerTurn == 0) {
                paintAndSetValues(btn,PLAYER_ONE_INDICATOR );
                currentPlayerTurn =  1;
                tvCurrentPlay.setText(String.valueOf(PLAYER_TWO_INDICATOR));

            } else {
                paintAndSetValues(btn, PLAYER_TWO_INDICATOR );
                currentPlayerTurn =  0;
                tvCurrentPlay.setText(String.valueOf(PLAYER_ONE_INDICATOR));

            }
            insertAndUpdateGame(UPDATE_GAME_STATE);
            gameState.setCurrentTurn(currentPlayerTurn);
        }
        Character winner = checkWinner(gameMatrix);
        if(winner !=null){
            Toast.makeText(getApplicationContext(), "El juego ha terminado, ganó "+winner, Toast.LENGTH_LONG).show();
            updateUsersAndGameState(winner);
            restartGame();
        }else if(winner == null && isMatrixFull()){
            Toast.makeText(getApplicationContext(), "El juego ha terminado, Empate ", Toast.LENGTH_LONG).show();
            updateUsersAndGameState(null);
            restartGame();
        }else{
            initTimer();
        }
    }

    private boolean isMatrixFull() {
        boolean result = false;
        int countFull = 0;

        for(int x = 0 ; x < gameMatrix.length ; x++){
            for(int i = 0 ; i < gameMatrix.length ; i++){
                if(gameMatrix[x][i]!=null) {
                    if(gameMatrix[x][i]!=null){
                        countFull++;
                       // break;
                    }

                }
            }
        }
        if(countFull == 9)
            result = true;

        return result;
    }

    private void updateUsersAndGameState(Character winner) {

        if (winner !=null){
            if(winner.equals(PLAYER_ONE_INDICATOR)){
                playerOne.setWin(playerOne.getWin()+1);
                playerTwo.setLost(playerTwo.getLost()+1);
               QueryUtils.updatePlayer(getApplicationContext(), playerOne, playerOne.getWin(),playerOne.getLost());
               QueryUtils.updatePlayer(getApplicationContext(), playerTwo, playerTwo.getWin(),playerTwo.getLost());
            }else{
                playerOne.setLost(playerOne.getLost() + 1);
                playerTwo.setWin(playerTwo.getWin() + 1);
                QueryUtils.updatePlayer(getApplicationContext(), playerOne, playerOne.getWin(),playerOne.getLost());
                QueryUtils.updatePlayer(getApplicationContext(), playerTwo, playerTwo.getWin(), playerTwo.getLost());
            }
        }

        QueryUtils.deleteGameState(getApplicationContext(), gameState);
        gameState = null;
        restartGame();

        gameState = insertAndUpdateGame(INSERT_GAME_STATE);
        checkParamsAndLoadGame(false);
    }

    private void restartGame() {
        for(int x = 0 ; x < gameMatrixButton.length ; x++){
            for(int i = 0 ; i < gameMatrixButton.length ; i++){
                gameMatrixButton[x][i].setText("");
            }
        }
        gameMatrix = new Character[3][3];
        currentPlayerTurn = 0;
    }

    private void paintAndSetValues(Button btn, Character playerInd){
        String keys =(String) btn.getTag();
        String keysSplit[] = keys.split("-");
        int xpos = Integer.parseInt(keysSplit[0]);
        int ypos = Integer.parseInt(keysSplit[1]);

        btn.setText(playerInd+"");

        gameMatrix[xpos][ypos] = playerInd;



    }
    private boolean canWrite(Button btntemp){
        boolean result = false;
        if(btntemp.getText().toString().isEmpty()){
            result = true;
        }
        return result;

    }

    public static Character checkWinner(Character[][] tablero){

        Character winner=null;
        if(GameHelper.isWinner(PLAYER_ONE_INDICATOR, tablero)) {
            winner='X';
        }
        if(GameHelper.isWinner(PLAYER_TWO_INDICATOR, tablero)){
            winner='O';
        }
        return winner;

    }


    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
        stopService(new Intent(getApplicationContext(), GameServiceOX.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Intent startServiceIntent = new Intent(getApplicationContext(), GameServiceOX.class);
        startService(startServiceIntent);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent); // now the rest of your code can use getIntent() as before
        //handleIntent(); // this should also be called from onCreate as well
    }
}
