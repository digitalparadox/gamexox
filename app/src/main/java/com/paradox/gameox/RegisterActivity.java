package com.paradox.gameox;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.paradox.gameox.model.GameState;
import com.paradox.gameox.model.Player;
import com.paradox.gameox.provider.DatabaseHelper;
import com.paradox.gameox.utils.QueryUtils;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = DatabaseHelper.class.getSimpleName();

    private Button btnPlay;
    private EditText etNickName1;
    private EditText etNickName2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initComponents();
        GameState gameState = QueryUtils.getGameState(getApplicationContext());

        if(gameState!=null){
            Intent intent = new Intent(this, PlayActivity.class);
            intent.putExtra("gameState", gameState);
            startActivity(intent);
        }
    }




    private void initComponents() {
        btnPlay = (Button )findViewById(R.id.btnPlay);
        etNickName1 = (EditText)findViewById(R.id.etNickname1);
        etNickName2 = (EditText)findViewById(R.id.etNickname2);

        btnPlay.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnPlay){
            if(isValidForm()) {

                Player p1 = QueryUtils.findPlayerByNicknameOrId(getApplicationContext(), etNickName1.getText().toString(), String.class);
                Player p2 = QueryUtils.findPlayerByNicknameOrId(getApplicationContext(), etNickName2.getText().toString(), String.class);

                if(p1 ==null){
                    long idp1 = QueryUtils.insertPlayer(getApplicationContext(), etNickName1.getText().toString(), "#4c4c4c");
                    if(idp1 > 0) {
                        p1 = QueryUtils.findPlayerByNicknameOrId(getApplicationContext(), etNickName1.getText().toString(), String.class);
                    }
                }
                if(p2 ==null){
                    long idp2 = QueryUtils.insertPlayer(getApplicationContext(), etNickName2.getText().toString(), "#4f4f4f");
                    if(idp2 > 0) {
                        p2 = QueryUtils.findPlayerByNicknameOrId(getApplicationContext(), etNickName2.getText().toString(), String.class);
                    }
                }


                Intent intent = new Intent(RegisterActivity.this, PlayActivity.class);
                intent.putExtra("playerOne", p1);
                intent.putExtra("playerTwo", p2);
                startActivity(intent);
            }else {
                Toast.makeText(getApplicationContext(), "Complete la información", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean isValidForm(){
        return (!isEmpty(etNickName1) && !isEmpty(etNickName2));

    }
    private boolean isEmpty(EditText et){
        return et.getText().toString().isEmpty();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
               super.onResume();
        App.activityResumed();
        stopService(new Intent(getApplicationContext(), GameServiceOX.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }



}
