package com.paradox.gameox;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.paradox.gameox.model.GameState;
import com.paradox.gameox.utils.QueryUtils;

public class GameServiceOX extends Service {

    private NotificationManager mNotificationManager;

    public GameServiceOX() {

    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            //notificationUtils = new NotificationUtils((NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE), this, intent);
            Log.i(getClass().getName().toString(), "Received start id " + startId + ": " + intent);

            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(!App.isActivityVisible()){
                        SystemClock.sleep(100000);
                        GameState gameState =  QueryUtils.getGameState(getApplicationContext());
                        if(gameState!= null)
                            sendNotification("Tienes un juego pendiente por terminar");
                    }
                }
            });
            t.start();


        } catch (Exception e) {
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }



    public static final int notifyID = 9001;
    NotificationCompat.Builder builder;

    private void sendNotification(String msg) {
        Intent resultIntent = new Intent(this, RegisterActivity.class);
        resultIntent.putExtra("msg", msg);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                resultIntent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder mNotifyBuilder;
        NotificationManager mNotificationManager;

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotifyBuilder = new NotificationCompat.Builder(this)
                .setContentTitle("Eyyy!! Regresa")
                .setContentText("Tienes un juego pendiente por terminar").setSmallIcon(R.mipmap.ic_launcher);
        // Set pending intent
        mNotifyBuilder.setContentIntent(resultPendingIntent);

        // Set Vibrate, Sound and Light
        int defaults = 0;
        defaults = defaults | Notification.DEFAULT_LIGHTS;
        defaults = defaults | Notification.DEFAULT_VIBRATE;
        defaults = defaults | Notification.DEFAULT_SOUND;

        mNotifyBuilder.setDefaults(defaults);
        // Set the content for Notification
        mNotifyBuilder.setContentText("Tienes un juego pendiente por terminar");
        // Set autocancel
        mNotifyBuilder.setAutoCancel(true);
        // Post a notification
        mNotificationManager.notify(notifyID, mNotifyBuilder.build());
    }
}
