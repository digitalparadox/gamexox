/*
 * Copyright 2015 Tinbytes Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.paradox.gameox.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;

public class SimpleContentProvider extends ContentProvider {
    // Provide a mechanism to identify all the incoming uri patterns.
    private static final int PLAYERS = 1;
    private static final int GAME_STATE = 2;
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        // /PLAYERS
        uriMatcher.addURI(DatabaseContract.AUTHORITY, DatabaseContract.PlayerTable.URI_PATH, PLAYERS);
        // /GAME STATE
        uriMatcher.addURI(DatabaseContract.AUTHORITY, DatabaseContract.GameStateTable.URI_PATH, GAME_STATE);

    }

    private DatabaseHelper dh;

    public boolean onCreate() {
        dh = new DatabaseHelper(getContext());
        return true;
    }

    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        Cursor c;
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setDistinct(true);
        switch (uriMatcher.match(uri)) {
            case PLAYERS:
                qb.setTables(DatabaseContract.PlayerTable.TABLE_NAME);

                if (sortOrder == null)
                    sortOrder = DatabaseContract.PlayerTable.CREATED_ON + " ASC";

                c = qb.query(dh.getReadableDatabase(), projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case GAME_STATE:
                qb.setTables(DatabaseContract.GameStateTable.TABLE_NAME);
                if (sortOrder == null)
                    sortOrder = DatabaseContract.GameStateTable.ID + " ASC";
                c = qb.query(dh.getReadableDatabase(), projection, selection, selectionArgs, null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (getContext() != null) {
            c.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return c;
    }

    private Uri doInsert(Uri uri, String tableName, Uri contentUri, ContentValues contentValues) {
        SQLiteDatabase db = dh.getWritableDatabase();
        long rowId = db.insert(tableName, null, contentValues);
        if (rowId > 0) {
            Uri insertedUri = ContentUris.withAppendedId(contentUri, rowId);
            if (getContext() != null) {
                getContext().getContentResolver().notifyChange(insertedUri, null);
            }
            return insertedUri;
        }
        throw new SQLException("Failed to insert row - " + uri);
    }

    public Uri insert(@NonNull Uri uri, ContentValues values) {
        if (values != null) {
            switch (uriMatcher.match(uri)) {
                case PLAYERS:
                    return doInsert(uri, DatabaseContract.PlayerTable.TABLE_NAME, DatabaseContract.PlayerTable.CONTENT_URI, values);
                case GAME_STATE:
                    return doInsert(uri, DatabaseContract.GameStateTable.TABLE_NAME, DatabaseContract.GameStateTable.CONTENT_URI, values);
                default:
                    throw new IllegalArgumentException("Unknown URI " + uri);
            }
        }
        return null;
    }

    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dh.getWritableDatabase();
        int count;
        switch (uriMatcher.match(uri)) {
            case PLAYERS:
                count = db.update(DatabaseContract.PlayerTable.TABLE_NAME, values, selection, selectionArgs);
                break;
            case GAME_STATE:
                count = db.update(DatabaseContract.GameStateTable.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dh.getWritableDatabase();
        int count;
        switch (uriMatcher.match(uri)) {
            case PLAYERS:
                StringBuilder in = new StringBuilder("(");
                Cursor c = db.query(DatabaseContract.PlayerTable.TABLE_NAME, null, selection, selectionArgs, null, null, null);
                boolean moveToNext = c.moveToNext();
                while (moveToNext) {
                    in.append(c.getInt(c.getColumnIndex(DatabaseContract.PlayerTable.ID)));
                    moveToNext = c.moveToNext();
                    if (moveToNext) {
                        in.append(",");
                    }
                }
                in.append(")");
                c.close();
                count = db.delete(DatabaseContract.PlayerTable.TABLE_NAME, selection, selectionArgs);
                if (count > 0) {
                    //count = db.delete(DatabaseContract.NoteLabelTable.TABLE_NAME, DatabaseContract.NoteLabelTable.NOTE_ID + " IN " + in, null);
                }
                break;
            case GAME_STATE:
                in = new StringBuilder("(");
                c = db.query(DatabaseContract.GameStateTable.TABLE_NAME, null, selection, selectionArgs, null, null, null);
                moveToNext = c.moveToNext();
                while (moveToNext) {
                    in.append(c.getInt(c.getColumnIndex(DatabaseContract.GameStateTable.ID)));
                    moveToNext = c.moveToNext();
                    if (moveToNext) {
                        in.append(",");
                    }
                }
                in.append(")");
                c.close();
                count = db.delete(DatabaseContract.GameStateTable.TABLE_NAME, selection, selectionArgs);
                if (count > 0) {
                    // count = db.delete(DatabaseContract.NoteLabelTable.TABLE_NAME, DatabaseContract.NoteLabelTable.LABEL_ID + " IN " + in, null);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case PLAYERS:
                return DatabaseContract.PlayerTable.CONTENT_TYPE;
            case GAME_STATE:
                return DatabaseContract.GameStateTable.CONTENT_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }
}
