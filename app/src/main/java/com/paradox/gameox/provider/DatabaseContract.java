/*
 * Copyright 2015 Tinbytes Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.paradox.gameox.provider;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseContract {
  public static final String AUTHORITY = "com.paradox.gameox.provider.SimpleContentProvider";
  public static final String DB_NAME = "gameoxcontentprovider.db";
  public static final int DB_VERSION = 1;

  public interface PlayerColumns {
    String ID = BaseColumns._ID;
    String NICKNAME = "nickname";
    String COLOR = "color";
    String CHARACTER = "character";
    String WIN = "win";
    String LOST = "lost";
    String CREATED_ON = "created_on";
  }

  public static final class PlayerTable implements PlayerColumns {
    public static final String TABLE_NAME = "player";
    public static final String URI_PATH = "player";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + URI_PATH);
    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + URI_PATH;

    private PlayerTable() {
    }
  }

  public interface GameStateColumns {
    String ID = BaseColumns._ID;
    String PLAYER_ONE_ID = "player_one";
    String PLAYER_TWO_ID = "player_two";
    String POS_0 = "posicion_0";
    String POS_1 = "posicion_1";
    String POS_2 = "posicion_2";
    String POS_3 = "posicion_3";
    String POS_4 = "posicion_4";
    String POS_5 = "posicion_5";
    String POS_6 = "posicion_6";
    String POS_7 = "posicion_7";
    String POS_8 = "posicion_8";
    String CREATED_ON = "created_on";
    String CURRENT_TURN = "current_turn";

  }

  public static final class GameStateTable implements GameStateColumns {
    public static final String TABLE_NAME = "gamestate";
    public static final String URI_PATH = "gamestate";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + URI_PATH);
    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + URI_PATH;

    private GameStateTable() {
    }
  }

}
