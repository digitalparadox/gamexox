/*
 * Copyright 2015 Tinbytes Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.paradox.gameox.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
  private static final String TAG = DatabaseHelper.class.getSimpleName();

  public DatabaseHelper(Context context) {
    super(context, DatabaseContract.DB_NAME, null, DatabaseContract.DB_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    // player
    db.execSQL("CREATE TABLE " + DatabaseContract.PlayerTable.TABLE_NAME + " (" +
        DatabaseContract.PlayerTable.ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
        DatabaseContract.PlayerTable.NICKNAME + " TEXT NOT NULL," +
        DatabaseContract.PlayerTable.COLOR + " TEXT NOT NULL," +
        DatabaseContract.PlayerTable.CHARACTER + " TEXT," +
        DatabaseContract.PlayerTable.WIN + " INTEGER NOT NULL," +
        DatabaseContract.PlayerTable.LOST + " INTEGER NOT NULL," +
        DatabaseContract.PlayerTable.CREATED_ON + " INTEGER NOT NULL)");

    // GameState
    db.execSQL("CREATE TABLE " + DatabaseContract.GameStateTable.TABLE_NAME + " (" +
        DatabaseContract.GameStateColumns.ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
        DatabaseContract.GameStateColumns.PLAYER_ONE_ID + " INTEGER NOT NULL," +
        DatabaseContract.GameStateColumns.PLAYER_TWO_ID + " INTEGER NOT NULL," +
        DatabaseContract.GameStateColumns.POS_0 + " TEXT," +
        DatabaseContract.GameStateColumns.POS_1 + " TEXT," +
        DatabaseContract.GameStateColumns.POS_2 + " TEXT," +
        DatabaseContract.GameStateColumns.POS_3 + " TEXT," +
        DatabaseContract.GameStateColumns.POS_4 + " TEXT," +
        DatabaseContract.GameStateColumns.POS_5 + " TEXT," +
        DatabaseContract.GameStateColumns.POS_6 + " TEXT," +
        DatabaseContract.GameStateColumns.POS_7 + " TEXT," +
        DatabaseContract.GameStateColumns.POS_8 + " TEXT," +
        DatabaseContract.GameStateColumns.CREATED_ON + " INTEGER NOT NULL," +
        DatabaseContract.GameStateColumns.CURRENT_TURN + " INTEGER NOT NULL)");


  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.PlayerTable.TABLE_NAME);
    db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.GameStateTable.TABLE_NAME);
   // db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.NoteLabelTable.TABLE_NAME);
    onCreate(db);
  }
}
