package com.paradox.gameox.utils;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.paradox.gameox.model.GameState;
import com.paradox.gameox.model.Player;
import com.paradox.gameox.provider.DatabaseContract;
import com.paradox.gameox.provider.DatabaseHelper;

import java.util.Date;

/**
 * Created by estebanlopez on 8/25/15.
 */
public class QueryUtils {

    private static final String TAG = DatabaseHelper.class.getSimpleName();

    public static Player findPlayerByNicknameOrId(Context context, Object params, Class<?> cls){



        String columnQuery = "";
        String[] selectionArgs = new String[1];

        if (cls == String.class) {
            selectionArgs[0] = (String)params;
            columnQuery = DatabaseContract.PlayerTable.NICKNAME + "=?";
        }
        else if (cls == int.class) {
            selectionArgs[0] = String.valueOf((int)params);
            columnQuery = DatabaseContract.PlayerTable.ID + "=?";

        }

        Cursor c = context.getContentResolver().query(DatabaseContract.PlayerTable.CONTENT_URI, null, columnQuery, selectionArgs, null);

        Player player = null;
        if (c != null) {


            while (c.moveToNext()) {
                player = new Player();
                int id = c.getInt(c.getColumnIndex(DatabaseContract.PlayerTable.ID));
                player.setId(id);
                if(c.getString(c.getColumnIndex(DatabaseContract.PlayerTable.CHARACTER)) != null){
                    player.setCharacter(c.getString(c.getColumnIndex(DatabaseContract.PlayerTable.CHARACTER)).charAt(0));
                }

                player.setColor(c.getString(c.getColumnIndex(DatabaseContract.PlayerTable.COLOR)));
                player.setNickname(c.getString(c.getColumnIndex(DatabaseContract.PlayerTable.NICKNAME)));
                player.setWin(c.getInt(c.getColumnIndex(DatabaseContract.PlayerTable.WIN)));
                player.setLost(c.getInt(c.getColumnIndex(DatabaseContract.PlayerTable.LOST)));
                player.setCreated_on(new Date(c.getLong(c.getColumnIndex(DatabaseContract.PlayerTable.CREATED_ON))));
                Log.d(TAG, "Player Id: " + id);
            }
            c.close();
        }

        Log.d(TAG, "********** Query USER ************");

        return player;
    }

    public static GameState getGameState(Context context){

        GameState gameState = null;
        Cursor c = context.getContentResolver().query(DatabaseContract.GameStateTable.CONTENT_URI, null, null, null, null);
        if (c != null) {


            while (c.moveToNext()) {
                gameState = new GameState();
                int id = c.getInt(c.getColumnIndex(DatabaseContract.GameStateTable.ID));

                gameState.setId(id);
                gameState.setPlayerOneId(c.getInt(c.getColumnIndex(DatabaseContract.GameStateTable.PLAYER_ONE_ID)));
                gameState.setPlayerTwoId(c.getInt(c.getColumnIndex(DatabaseContract.GameStateTable.PLAYER_TWO_ID)));
                gameState.setCreated_on(new Date(c.getLong(c.getColumnIndex(DatabaseContract.GameStateTable.CREATED_ON))));


                gameState.setPosO(getValueFromCursor(c, DatabaseContract.GameStateTable.POS_0));
                gameState.setPos1(getValueFromCursor(c, DatabaseContract.GameStateTable.POS_1));
                gameState.setPos2(getValueFromCursor(c, DatabaseContract.GameStateTable.POS_2));
                gameState.setPos3(getValueFromCursor(c, DatabaseContract.GameStateTable.POS_3));
                gameState.setPos4(getValueFromCursor(c, DatabaseContract.GameStateTable.POS_4));
                gameState.setPos5(getValueFromCursor(c, DatabaseContract.GameStateTable.POS_5));
                gameState.setPos6(getValueFromCursor(c, DatabaseContract.GameStateTable.POS_6));
                gameState.setPos7(getValueFromCursor(c, DatabaseContract.GameStateTable.POS_7));
                gameState.setPos8(getValueFromCursor(c, DatabaseContract.GameStateTable.POS_8));

                gameState.setCurrentTurn(c.getInt(c.getColumnIndex(DatabaseContract.GameStateTable.CURRENT_TURN)));


                Log.d(TAG, "Game State Id: " + id);

            }
            c.close();
        }

        return gameState;
    }

    public static void deleteGameState(Context context, GameState gameState){
      int rowsUpdated =   context.getContentResolver().delete(DatabaseContract.GameStateTable.CONTENT_URI,null, null);
    //  int rowsUpdated =   context.getContentResolver().delete(DatabaseContract.GameStateTable.CONTENT_URI, DatabaseContract.GameStateTable.ID + "=?", new String[]{String.valueOf(gameState.getId())});
        Log.d(TAG, "---DATA DELETED--- "+ rowsUpdated);
    }
    public static Character getValueFromCursor(Cursor cur, String posString) {
        Character character = null;
        String value = cur.getString(cur.getColumnIndex(posString));
        if (value == null) {
            character = null;
        }
        else if (value.equals("null")) {
            character = null;
        } else {
            character = value.charAt(0);
        }
        return character;
    }

    public static long insertPlayer(Context context, String nickname, String color){
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.PlayerTable.NICKNAME, nickname);
        values.put(DatabaseContract.PlayerTable.COLOR, color);
        values.put(DatabaseContract.PlayerTable.WIN, 0);
        //values.put(DatabaseContract.PlayerTable.CHARACTER, character);
        values.put(DatabaseContract.PlayerTable.LOST, 0);
        values.put(DatabaseContract.PlayerTable.CREATED_ON, new Date().getTime());
        long userId = ContentUris.parseId(context.getContentResolver().insert(DatabaseContract.PlayerTable.CONTENT_URI, values));

        return userId;
    }
    public static long insertGameState(Context context, Character [][] pGameMatrix,
                                            int playerOneId, int playerTwoId, int pCurrentTurn){
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.GameStateTable.PLAYER_ONE_ID,playerOneId);
        values.put(DatabaseContract.GameStateTable.PLAYER_TWO_ID,playerTwoId);
        values.put(DatabaseContract.GameStateTable.CREATED_ON, new Date().getTime());

        values.put(DatabaseContract.GameStateTable.POS_0,String.valueOf(valueReturnMat(pGameMatrix[0][0])));
        values.put(DatabaseContract.GameStateTable.POS_1,String.valueOf(valueReturnMat(pGameMatrix[0][1])));
        values.put(DatabaseContract.GameStateTable.POS_2,String.valueOf(valueReturnMat(pGameMatrix[0][2])));
        values.put(DatabaseContract.GameStateTable.POS_3,String.valueOf(valueReturnMat(pGameMatrix[1][0])));
        values.put(DatabaseContract.GameStateTable.POS_4,String.valueOf(valueReturnMat(pGameMatrix[1][1])));
        values.put(DatabaseContract.GameStateTable.POS_5,String.valueOf(valueReturnMat(pGameMatrix[1][2])));
        values.put(DatabaseContract.GameStateTable.POS_6,String.valueOf(valueReturnMat(pGameMatrix[2][0])));
        values.put(DatabaseContract.GameStateTable.POS_7,String.valueOf(valueReturnMat(pGameMatrix[2][1])));
        values.put(DatabaseContract.GameStateTable.POS_8,String.valueOf(valueReturnMat(pGameMatrix[2][2])));

        values.put(DatabaseContract.GameStateTable.CURRENT_TURN,pCurrentTurn);


        long gameId = ContentUris.parseId(context.getContentResolver().insert(DatabaseContract.GameStateTable.CONTENT_URI, values));

        return gameId;
    }
    private static Character valueReturnMat(Character character){
        return character == null ? 'n' : character;
    }
    public static void updateGameState(Context context, Character [][] pGameMatrix, int pCurrentTurn, GameState pgameState){
        ContentValues values = new ContentValues();

        values.put(DatabaseContract.GameStateTable.POS_0, String.valueOf(pGameMatrix[0][0]));
        values.put(DatabaseContract.GameStateTable.POS_1, String.valueOf(pGameMatrix[0][1]));
        values.put(DatabaseContract.GameStateTable.POS_2, String.valueOf(pGameMatrix[0][2]));
        values.put(DatabaseContract.GameStateTable.POS_3, String.valueOf(pGameMatrix[1][0]));
        values.put(DatabaseContract.GameStateTable.POS_4, String.valueOf(pGameMatrix[1][1]));
        values.put(DatabaseContract.GameStateTable.POS_5, String.valueOf(pGameMatrix[1][2]));
        values.put(DatabaseContract.GameStateTable.POS_6, String.valueOf(pGameMatrix[2][0]));
        values.put(DatabaseContract.GameStateTable.POS_7, String.valueOf(pGameMatrix[2][1]));
        values.put(DatabaseContract.GameStateTable.POS_8, String.valueOf(pGameMatrix[2][2]));

        values.put(DatabaseContract.GameStateTable.CURRENT_TURN, pCurrentTurn);

        int result = context.getContentResolver().update(DatabaseContract.GameStateTable.CONTENT_URI, values, DatabaseContract.GameStateTable.ID + "=" + pgameState.getId(), null);
        Log.d(TAG, "---DATA UPDATED---Rows "+result );
    }
     public static void updatePlayer(Context context, Player player, int pwin, int plost){
        ContentValues values = new ContentValues();

        values.put(DatabaseContract.PlayerTable.WIN, pwin);
        values.put(DatabaseContract.PlayerTable.LOST, plost);


        int result = context.getContentResolver().update(DatabaseContract.PlayerTable.CONTENT_URI, values, DatabaseContract.PlayerTable.ID + "=?",  new String[]{String.valueOf(player.getId())});
        Log.d(TAG, "---DATA USERS UPDATED---Rows "+result );
    }


}
